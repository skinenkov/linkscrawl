

from PyPDF2 import PdfFileReader
from linkscrawl.settings import logger
import hashlib
import re


links_reg = re.compile(r'(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?', re.I)


class ProcessingError(Exception):
    pass


class FormatError(Exception):
    pass


class FormFileProcessor():
    def __init__(self, filestream):
        if not hasattr(filestream, 'read'):
            raise FormatError('File is not like a file')
        self.file = filestream
        self.links = set()
        self.name = filestream.name
        self.hexdigest = hashlib.md5(filestream.read()).hexdigest()
        self.init_file()

    def init_file(self):
        self.pdfResource = PdfFileReader(self.file)

    @staticmethod
    def link_joiner(link):
        return '%s://%s%s' % link

    def extract_links(self):
        for p in range(self.pdfResource.getNumPages()):
            page = self.pdfResource.getPage(p)
            text = page.extractText()
            links = links_reg.findall(text)
            if links:
                links = [self.link_joiner(l) for l in links]
                self.links.update(links)
        logger.info('From %s extracted %s links.', self.name, len(self.links))
        return self.links

