
from django.views.generic import View
from django.http import HttpResponse, HttpResponseBadRequest
from django.core.exceptions import ObjectDoesNotExist
from pdf.pdflib import FormFileProcessor, ProcessingError, FormatError
from pdf.models import Files, Links
from linkscrawl.settings import logger
import json


class BaseView(View):

    def json_response(self, context):
        return HttpResponse(
            json.dumps(context), content_type='application/json')


class UploadHandler(BaseView):

    def get(self, request):
        return HttpResponse(
            json.dumps({'data': 'ok'}), content_type='application/json')

    def post(self, request, **kwargs):
        try:
            processor = FormFileProcessor(request.FILES['data'])
            existed = Files.objects.filter(hash=processor.hexdigest)
            if existed:
                links = Links.objects.filter(file=existed[0])
            else:
                file = Files(name=processor.name, hash=processor.hexdigest)
                file.save()
                links = set(processor.extract_links())
                if links:
                    Links.insert_many(file=file, links=links)
            row = {
                'name': processor.name,
                'links': len(links)
            }
        except (ProcessingError, FormatError), exc:
            return HttpResponseBadRequest(str(exc))
        else:
            return self.json_response({'data': 'ok', 'result': row})


class FileListHandler(BaseView):
    def get(self, request):
        return self.json_response(Files.get_list())


class LinksListHandler(BaseView):
    def get(self, request):
        return self.json_response(Links.get_list())


class FileUrlsHandler(BaseView):
    def get(self, request, fileid):
        try:
            file = Files.objects.get(id=fileid)
        except ObjectDoesNotExist:
            return self.json_response({'error': 'file not found'})
        else:
            links = [l.link for l in file.get_links()]
            return self.json_response({'result': links})


class FilesPerLinkHandler(BaseView):
    def get(self, request):
        return self.json_response(Links.per_file_list())

