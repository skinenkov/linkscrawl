

from django.db import models, transaction
from linkscrawl.settings import logger, ALIVE_CHECKER_INTERVAL
import urllib


class Files(models.Model):
    name = models.CharField(max_length=50)
    hash = models.CharField(max_length=50)

    @classmethod
    def get_list(cls):
        files = cls.objects.all()
        res = []
        for file in files:
            res.append({
                'id': file.id,
                'name': file.name,
                'links': len(Links.objects.filter(file=file))})
        return res

    def get_links(self):
        return Links.objects.filter(file=self)


class Links(models.Model):
    file = models.ForeignKey(Files, on_delete=models.CASCADE)
    link = models.CharField(max_length=80)
    alive = models.BooleanField(default=False)

    @classmethod
    def insert_many(cls, file, links):
        with transaction.atomic():
            for link in links:
                cls(link=link, file=file).save()
            logger.info('db inserted %s links.', len(links))

    @classmethod
    def get_list(cls):
        links = cls.objects.all()
        res = []
        for link in links:
            res.append({
                'link': link.link,
                'file': link.file.name,
                'is_alive': int(link.alive)
            })
        return res

    @classmethod
    def per_file_list(cls):
        res = []
        try:
            links = cls.objects.all().distinct('link')
            for link in links:
                res.append({
                    'link': link.link,
                    'contained_in': len(cls.objects.filter(
                        link=link.link).distinct('file'))
                })
        except NotImplementedError:
            # Sqlite has not distinct backend in django
            links = set([o.link for o in cls.objects.all()])
            for link in links:
                files = set([o.file for o in cls.objects.filter(link=link)])
                res.append({
                    'link': link,
                    'contained_in': len(files)
                })
        return res

    @classmethod
    def alive_checker(cls):
        import time
        while True:
            links = cls.objects.all()
            logger.info('alive check %s links', len(links))
            for link in links:
                try:
                    resp = urllib.urlopen(link.link)
                except Exception, exc:
                    logger.error(
                        'url open error %s \n %s', link.link, str(exc))
                else:
                    if resp.code >= 200 and resp.code < 300:
                        link.alive = True
                    else:
                        link.alive = False
                    link.save()
            time.sleep(ALIVE_CHECKER_INTERVAL)



