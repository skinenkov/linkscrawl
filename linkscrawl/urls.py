"""linkscrawl URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from pdf.views import (
    UploadHandler, FileListHandler, LinksListHandler,
    FilesPerLinkHandler, FileUrlsHandler
)

urlpatterns = [
    url(r'^upload/$', UploadHandler.as_view()),
    url(r'^files/$', FileListHandler.as_view()),
    url(r'^links/$', LinksListHandler.as_view()),
    url(r'^links/(?P<fileid>[0-9]{1,10})/$', FileUrlsHandler.as_view()),
    url(r'^linkperfile/$', FilesPerLinkHandler.as_view())
]
