

To start application:
1. Create virtualenv for 2.7 intepreter.
2. Activate it.
3. pip install -r requirements.txt
4. Run django: python manage.py runserver 8000


Usage:
1. Upload file example.pdf from current directory:
curl -X "POST" -H "X-CSRFToken:vGpifQR12BxT07moOohREGmuKp8HjxaE" -H "Content-Type: multipart/form-data" -F "data=@example.pdf"  --cookie "csrftoken=vGpifQR12BxT07moOohREGmuKp8HjxaE"  http://localhost:8000/upload/

You need to set header and cooke for django security on POST methods.

2. Get all files:

curl -X "GET" http://localhost:8000/files/

3. Get all links:

curl -X "GET" http://localhost:8000/links/

4. Get Files per link:

curl -X "GET" http://localhost:8000/linkperfile/

5. Get links of file:

curl -X "GET" http://localhost:8000/links/{fileid}/

where fileid - id field from 2) query.


